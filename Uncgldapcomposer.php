<?php
use Composer\Script\Event;

/**
 * Installer to be used when composer is installing the base app.
 */
class Uncgldapcomposer
{
    public static function setupSymlinks(Event $event)
    {
        $paths = self::_helperDirectory($event);

        $io = $event->getIO();
        
        if (!file_exists($paths['basePath'] . '/library/Internal/Auth/Adapter')) {
            mkdir($paths['basePath'] . '/library/Internal/Auth/Adapter', 0777, true);
        }
        
        $foldersToLink = array(
            '/library/Internal/Auth/Adapter/Uncgldap',
        );

        foreach ($foldersToLink as $f) {
            @unlink($paths['basePath'] . $f);
            symlink($paths['uncgAuthAdapterPath'], $paths['basePath'] . $f);
            $io->write('Symlinking ' . $f);
        }
        
    }

    public static function _helperDirectory(Event $event)
    {
        return array(
            'basePath'                  => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/../'),
            'uncgAuthAdapterPath'       => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/uncgits/uncg-ldap-otf/uncgldap')
        );
    }

}