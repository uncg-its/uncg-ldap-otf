<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
  * @package    Internal_Auth_Adapter_Uncgldap
 * @category   Authentication Adapter
 * @copyright  Copyright (c) 2013 UNCG ITS - Application Administration
 * @version    SVN: $Id: $
 */

/**
 * This adapter uses UNCG's LDAP service to authenticate a user.  
 *
 * @package    Internal_Auth_Adapter_Uncgldap
 * @category   Authentication Adapter
 * @copyright  Copyright (c) 2013 UNCG ITS - Application Administration
 */
class Internal_Auth_Adapter_Uncgldap_Uncgldapall implements Zend_Auth_Adapter_Interface, Ot_Auth_Adapter_Interface
{

    /**
     * Username of the user to authenticate
     *
     * @var string
     */
    protected $_username = '';

    /**
     * Password of the user to authenticate
     *
     * @var string
     */
    protected $_password = '';

    /**
     * Database adapter
     *
     * @var object
     */
    protected $_db = null;
    
    protected $_name = 'tbl_ot_account';

    /**
     * Constructor to create new object
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username = '', $password = '')
    {
        global $application;

        $prefix = $application->getOption('tablePrefix');

        if (!empty($prefix)) {
            $this->_name = $prefix . $this->_name;
        }
        
        $this->_username = $username;
        $this->_password = $password;
    }

    /**
     * Authenticates the user passed by the constructor.
     *
     * @return new Zend_Auth_Result object
     */
    public function authenticate()
    {
        
        $ldap_server = "ldaps://ldap.uncg.edu";
        $dn = 'cn=' . $this->_username . ', ou=accounts, o=uncg';

        $connect=@ldap_connect($ldap_server, 636);
        ldap_set_option($connect, LDAP_OPT_TIMELIMIT, 5);
        ldap_set_option($connect, LDAP_OPT_NETWORK_TIMEOUT, 5); 
        if ($connect) {
            $valid=@ldap_bind($connect,$dn,$this->_password);
        }
        
        if (!$valid) {
            return new Zend_Auth_Result(false, null, array('UNCG LDAP Authentication Failed.'));
        }
        
        $srch = @ldap_search($connect,
            'ou=accounts, o=uncg',
            'cn=' . $this->_username,
            array('uncgpreferredgivenname','uncgpreferredsurname','givenname','sn')
        );
        
        if (!$srch) {
            return new Zend_Auth_Result(false, null, array('UNCG LDAP Authentication Failed: Not Current Faculty or Staff.'));
        }
        
        $results = @ldap_get_entries($connect, $srch);
        
        if ( $results["count"] > 0) {
            
                $class = new stdClass();
                $class->username = $this->_username;
                $class->firstName = (isset($results[0]['uncgpreferredgivenname'][0])) ? $results[0]['uncgpreferredgivenname'][0] : $results[0]['givenname'][0];
                $class->lastName = (isset($results[0]['uncgpreferredsurname'][0])) ? $results[0]['uncgpreferredsurname'][0] : $results[0]['sn'][0];
                $class->emailAddress = $this->_username . '@uncg.edu';
                $class->timezone = 'America/New_York';
                $class->realm    = 'uncgldap';
            
        } else {
            return new Zend_Auth_Result(false, null, array('UNCG LDAP Authentication Failed.'));
        }
        
        ldap_close($connect);

        return new Zend_Auth_Result(true, $class, array());
    }

    /**
     * Sets the autologin to false so that it uses native login mechanism
     *
     * @return boolean
     */
    public static function autoLogin()
    {
        return false;
    }

    /**
     * Does nothing on logout since Zend_Auth handles it all
     *
     */
    public static function autoLogout()
    {
        Zend_Auth::getInstance()->clearIdentity();
    }

    /**
     * Flag to tell the app where the authenticaton is managed
     *
     * @return boolean
     */
    public static function manageLocally()
    {
        return false;
    }
    
    /**
     * URL to send people to for non-locally managed passwords
     *
     * @return boolean
     */
    public static function remotePasswordResetUrl()
    {
        return 'http://reset.uncg.edu';
    }

    /**
     * flag to tell the app whether a user can sign up or not
     *
     * @return boolean
     */    
    public static function allowUserSignUp()
    {
        return false;
    }    

}